
import { SkSpinnerImpl } from '../../sk-spinner/src/impl/sk-spinner-impl.js';

export class AntdSkSpinner extends SkSpinnerImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'spinner';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }
}
